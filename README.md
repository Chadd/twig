# TWIG - Teams Webhook Integration Gateway

## License
MIT license - see LICENSE file for full details

## Disclaimer
TWIG is not affiliated or endorsed by Microsoft Corporation. Microsoft Teams, Microsoft Flow and Office 365 are registered trademarks of Microsoft.

## Description
TWIG is an integration middleware designed to allow the triggering of Microsoft Flow or Microsoft Logic Apps from a Microsoft Teams Outbound Web Hook, aka; Custom Web Hook. TWIG does this by proxying the request format of an outgoing webhook from Teams to a truly asynchronus call to Flow/Logic Apps.