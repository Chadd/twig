const AWS = require('aws-sdk');             // AWS API
const s3 = new AWS.S3();                    // s3 bucket API
const db = new AWS.dynamodb();              // Doc storage via DynamoDB

exports.subscribe = function(event, context, callback) {
    callback(null, {body:"", statusCode: 202});
};

exports.unsubscribe = function(event, context, callback) {
    callback(null, {body:"", statusCode: 202});
};

exports.event = function(event, context, callback) {
    callback(null, {body:"", statusCode: 202});
};